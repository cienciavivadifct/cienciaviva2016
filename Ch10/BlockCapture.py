import pygame
import random

# ------------------------ Global constants ----------------------------------

# Colors
BLACK = (  0,   0,   0)
WHITE = (255, 255, 255)
BLUE  = (  0,   0, 255)
GREEN = (  0, 255,   0)
RED   = (255,   0,   0)

# Screen dimensions
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

# --------------------------- Classes ----------------------------------------

# Collectable blocks
class Block(pygame.sprite.Sprite):
    def __init__(self):
        # Call parent constructor
        super().__init__()
        
        # Create blank surface to draw on.
        self.image = pygame.Surface([20, 20]) # blank surface
        self.image.fill(BLACK) # paint the surface
        
        self.rect = self.image.get_rect()
    
    def resetPos(self):
        self.rect.y = random.randrange(-300, -20)
        self.rect.x= random.randrange(SCREEN_WIDTH)
    
    def update(self):
        """ Called each frame. """
         
        # Move block down one pixel
        self.rect.y += 1
        
        if self.rect.y > SCREEN_HEIGHT + self.rect.height:
            self.resetPos()
            
class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface([20,20])
        self.image.fill(RED)
        self.rect = self.image.get_rect()
    
    def update(self):
        pos = pygame.mouse.get_pos()
        self.rect.x = pos[0]
        self.rect.y = pos[1]

class Game():
    # Class attributes
    # Sprite lists
    blocksList = None
    allSpritesList = None
    player = None
    gameOver = False
    
    # Other game data
    score = 0
    
    # Class methods:
    # Constructor
    def __init__(self):
        self.score = 0
        self.gameOver = False
        
        # Create the sprites lists. At this point, they are empty
        self.blocksList = pygame.sprite.Group()
        self.allSpritesList = pygame.sprite.Group()
        
        # Create and add sprites to the lists.
        for i in range (50):
            block = Block()
            block.resetPos()
        
            self.blocksList.add(block)
            self.allSpritesList.add(block)
        
        # Now, create the player and add it to the sprites list
        self.player = Player()
        self.allSpritesList.add(self.player)
        
    # Process all events. Return true if we need to close the window.
    def processEvents(self):
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                return True

            if event.type == pygame.MOUSEBUTTONDOWN:
                if self.gameOver:
                    self.__init__()
        
        return False

    # This method is run each time the the game cycle repeats it updates 
    # positions and checks for collisions.
    def runLogic(self):
        
        if not self.gameOver:
            # Move all the sprites
            self.allSpritesList.update()
            
            # See if hte player block has collided with anything.
            blocksHitList = pygame.sprite.spritecollide(self.player, self.blocksList, True)
            
            # Check the list of collisions
            for block in blocksHitList:
                self.score += 1
                print (self.score)
            
            if len (self.blocksList) == 0:
                self.gameOver = True
    
    # Display everything on the game screen.
    def displayFrame(self, screen):
        
        screen.fill(WHITE)
        
        if self.gameOver:
            font = pygame.font.SysFont("serif", 25)
            text = font.render("Game over, click to restart", True, BLACK)
            x = (SCREEN_WIDTH // 2) - (text.get_width() // 2)
            y = (SCREEN_HEIGHT // 2) - (text.get_height() // 2)
            screen.blit(text, [x, y])
        
        else: # the game is not over yet!
            self.allSpritesList.draw(screen)
        
        pygame.display.flip()

# Main function.

def main():
    
    # initiate game engine
    pygame.init()
    
    # setup window
    size = [SCREEN_WIDTH, SCREEN_HEIGHT]
    screen = pygame.display.set_mode(size)
    
    pygame.display.set_caption("Block capture awesome game")
    pygame.mouse.set_visible(False)
    
    # initialize objects and game data.
    done = False
    clock = pygame.time.Clock()
    game = Game()
    
    # Main game loop
    while not done:
        done = game.processEvents()
        game.runLogic()
        game.displayFrame(screen)
        clock.tick(60)
    
    pygame.quit()

if __name__ == "__main__":
    main()