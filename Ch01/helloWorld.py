# Examples on how to print messages in the console

# Hello world
print "Hello world!"

# Printing the results of an expression 
print "2 + 4"
print 3+5
print "Inhabitants in Snow White's house", 7 + 1
print "Inhabitants in Snow White's house", "7 + 1"
print "Inhabitants in Snow White's house,", "7 + 1"


