from random import randint

class CriaturaMarinha():
    def __init__(self, x, y, peso):
        self.x = x
        self.y = y
        self.peso = peso
        self.tipo = "um qualquer"

    def estado(self):
        print self.tipo, "@", self.x, ", ", self.y

class Peixe(CriaturaMarinha):
    def __init__(self, x, y, peso, velX, velY):
        CriaturaMarinha.__init__(self, x, y, peso)
        self.velX = velX
        self.velY = velY
        self.tipo = "peixe"
    
    def nada(self):
        self.x = self.x + self.velX
        self.y = self.y + self.velY

class Tubarao(Peixe):
    def __init__(self, x, y, peso, velX, velY):
        Peixe.__init__(self, x, y, peso, velX, velY)
        self.tipo = "tubarao"
    
    def estado(self):
        print "Sou um tubarao comilao, peso", self.peso, "@", self.x, self.y
        
    def alteraPosicao(self):
        self.x = input("posicao x: ")
        self.y = input("posicao y: ")

    def comerUm(self, cardume):
        self.peso = self.peso + cardume.comerUm(self.x, self.y)


class Cardume():
    def __init__(self, numeroPeixes):
        self.peixinhos = [] # Cardume vazio
        contador = 0
        while contador < numeroPeixes:
            x = randint(0, 10)
            y = randint(0, 20)
            peso = randint(1, 5)
            velX = randint(-2, 2)
            velY = randint(-2, 2)
            self.peixinhos.append(Peixe(x, y, peso, velX, velY))
            contador = contador + 1

    def mostraPeixes(self):
        for cadapeixe in self.peixinhos:
            cadapeixe.estado()
    
    def comerUm(self, posX, posY):
        indice = 0
        while indice < len( self.peixinhos):
            if self.peixinhos[indice].x == posX and self.peixinhos[indice].y == posY:
                peso = self.peixinhos[indice].peso
                del self.peixinhos[indice]
                return peso
            else:
                indice = indice + 1
        return 0      

# o meu programa...
peixes = Cardume( 5 )
bruce = Tubarao( 10, 0, 100, 1, 1 )

while True:
    bruce.estado()
    peixes.mostraPeixes()
    bruce.alteraPosicao()
    bruce.comerUm( peixes )



######
criatura = CriaturaMarinha( 2, 5, 2 )
peixe1 = Peixe( 2, 2, 1, 1, 0 )
criatura.estado()
peixe1.estado()
peixe1.nada()

bruce = Tubarao( 10, 0, 100, 1, 1 )
bruce.estado()
mike = Tubarao( 20, 20, 150, 0.5, 0.5 )
mike.estado()
