
# as minhas definicoes

def imprimeLista( lista ):
    i = 0
    while i < len(lista):
        print lista[i]
        i = i + 1

def imprimeL( lista ):
    for elem in lista:
        print elem

def imprimeContrario( lista ):
    i = len( lista )-1
    while i >= 0:
        print lista[i]
        i = i - 1

# o meu programa

listaCompras = ["bananas", "tv", "peras","cebolas", "cenouras"]

print "primeiro elemento:", listaCompras[0]
print "a lista tem", len(listaCompras), "itens"

print "a minha lista:"
imprimeL( listaCompras )
listaCompras.append("telemovel")
print "a minha nova lista:"
imprimeL( listaCompras )

listaCompras.remove("peras")
listaCompras.sort()
imprimeL( listaCompras )
listaCompras.reverse()
imprimeL( listaCompras )
#imprimeContrario( listaCompras )

del listaCompras[0]
del listaCompras[0]
print "lista depois de del"
imprimeL(listaCompras)




