
from math import sqrt

class Vet:

    def __init__( self, x, y ):
        self.x = x
        self.y = y

    def imprime(self):
        print "(", self.x, ",", self.y, ")"

    def soma(self, v ):
        self.x = self.x + v.x
        self.y = self.y + v.y

    def igual( self, v ):
        return self.x == v.x and self.y == v.y

    def comprimento(self):
        return sqrt(self.x*self.x + self.y*self.y)

###

def imprimeAstro( lista ):
    for elem in lista:
        elem.imprime()
####

astros = [ ]

astros.append( Vet( 0, 1 ) )
astros.append( Vet( 10, 5 ) )
astros.append( Vet( 2, 8 ) )

imprimeAstro( astros )

print "comp. de astros[0]: ", astros[0].comprimento()


###
#v1.imprime()
#v1.soma( v2 )
#v1.imprime()
#v1.soma( v3 )
#v1.imprime()

#v4 = v3
#if v4.igual( v3 ):
#    print "v4 == v3"
#else:
#    print " diferentes"




