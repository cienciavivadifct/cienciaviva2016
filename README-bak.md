![FCTUNL](http://www.fct.unl.pt/sites/default/themes/fct_unl_pt_2015/images/logo.png)

# [Departamento de Informática](http://www.di.fct.unl.pt)

# Game Programming with Python - FCT

Fernando Birra (fpb@fct.unl.pt), João Costa Seco (joao.seco@fct.unl.pt) and Miguel Goulão (mgoul@fct.unl.pt)

![Python](https://www.python.org/static/community_logos/python-logo-master-v3-TM.png)

In this activity we invite you to understand the nuts and bolts of a computer game and learn some basic programming concepts along the way. We challenge you to follow a sequence of simple steps that will guide you from the basic elements of a program written in the Python programming language, to the construction of a little computer game, reusing a set of available components.

# Programming 101

Let us start by talking a bit about programming. Is that even possible, in such a short amount of time?
To build a program is, essentially, to specify the behavior of a computer, in such a way that it fulfills our purposes. We use a programming language to write this specification. With this language, we are able to express this behavior using concepts like values, variables, functions, objects and classes. In this short tutorial, we will use the Python programming language. You can download Python tools from [www.python.org](https://www.python.org).

Most modern programs and computer systems are built from pre-fabricated software components, which are used as building blocks to create new programs. These building blocks are, themselves, software components. The development of a new program often involves a combination of pre-fabricated components with new components.

This week, we will do just that. We will start from the basics of programming. As we progress, we will take on increasingly complex challenges. In the end, we will use an existing "game engine" -- a set of pre-fabricated components -- to build... games! The game engine is called Panda3D and may be obtained from [www.panda3d.org](http://www.panda3d.org). 

But first things first...

## Printing messages on the console

### Hello, world!

The classic example of a simple program in any programming language is the famous Hello World. It goes like this: when you run the program, it prints the `"Hello, world!"` message on the screen.

~~~~
Hello, world!
~~~~

How can we do this with Python? It turns out it is quite simple to do it. You will need to use the `print` command. The `print` command prints something on the output (in this case, on a console). It receives one argument, with whatever you want to print. So, if you want to print the message `"Hello, world!"`, that is exactly what you need to provide as argument for the `print` command. In this case, the argument is a bit of text. We represent text as a **String**, in **Python**. The String is delimited by double quotes `"`.

To try this, open a python console. To do so, open a command shell and call the command `ppython`. You should see a console with a text similar to this one (version details may be slightly different on your machine):

~~~~
Python 2.7.10 (default, May 23 2015, 09:44:00) [MSC v.1500 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>>
~~~~

The `>>>` prompts the user to type in a command. Let us try out the `print` command.

~~~~
Python 2.7.10 (default, May 23 2015, 09:44:00) [MSC v.1500 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
>>> print "Hello, world!"
Hello, world!
~~~~



### Printing the results of an expression

As the print command prints whatever is in between those quotes, if you write `print "2 + 4"`, you will obtain as a result `2 + 4`, rather than `6`. What if you really want to print the result of an expression, such as `2 + 4`? Remember, the print operation prints what is inside of its arguments list. "Hello, world!", or "2 + 4", were expressions of the type String. You can use other expression types, such as expressions computing an integer value. To do so, simply remove the double quotes, as in the following example:

~~~~
>>> print "2 + 4"
2 + 4
>>> print 2 + 4
6
~~~~

In this case, the result to be printed is `6`. This is computed as follows: first, **Python** computes the value of the expression. Then, it prints that value. Of course, the expression must be a valid expression. For example, if you try to print `Hello, world!` without the double quotes, it does not work. **Python** does not know how to interpret this, so if you try it, you will get a message like this:

~~~~
>>> print Hello, world!
  File "<stdin>", line 1
    print Hello, world!
                      ^
SyntaxError: invalid syntax
~~~~

If you are programming, every now and then these things will happen. 

**DON'T PANIC!!! :-)**

This is just the way Python has of telling you it does not understand the instruction you wrote. Computer programs have to be written following strict syntax rules. If we do not follow those rules, we will get error messages such as the one above.

### Printing multiple items

Sometimes, you will want to print several items. You can do so by separating those items with commas, in the print instruction. Let us look at a few examples. Suppose you want to print a message concerning how many people live in Snow White's house. There are 7 dwarves and Snow White, there. You can have python doing the math for you. An important detail is that the comma separating the arguments must be outside of the string. If you want a comma inside the string, you still need comma separating arguments outside of it (i.e. outside the double quotes).

~~~~
>>> print "Inhabitants in Snow White's house", 7 + 1
Inhabitants in Snow White's house 8
>>> print "Inhabitants in Snow White's house,", 7 + 1
Inhabitants in Snow White's house 8
>>> print "Inhabitants in Snow White's house,", 7 + 1, "and", 2, "little birds"
Inhabitants in Snow White's house, 8 and 2 little birds
>>> print "Inhabitants in Snow White's house," 7 + 1
  File "<stdin>", line 1
    print "Inhabitants in Snow White's house," 7 + 1
                                               ^
SyntaxError: invalid syntax
~~~~

You may also want to use special characters in your text. For example, you may want to write `You know what they say: "it ain't over until the fat lady sings..."`. But the double quote, as we have seen, is used as a delimiter for strings.
I How can we put it *inside* the string? We need to use an escape code. Here is how it works. You use a special combination of charaters to denote the one you really want to write. For example, for printing `"` you need to write `\"`. Putting it all together, here is what you get:

~~~~
>>> print "You know what they say: \"it ain't over until the fat lady sings...\""
You know what they say: "it ain't over until the fat lady sings..."
~~~~

There are several escape characters. For instance, use `\'` for a single quote `'`, `\"` for a double quote, `\t`, for a Tab, `\r`for a carriage return, or `\n` for a line feed (i.e., for changing to the next line). Play a bit with them to see their effect.

Some of these special characters are using differently depending on the operating system. For example, in Windows, the change of line is denoted by `\r\n`, while on Linux, and Mac OS this is denoted by `\n` and, in older Mac systems, this was denoted by `\r`.

## Saving your programs

So far, we have been playing with python on the console. This is convenient for trying out a few expressions, but not a good idea if you want build more complex programs. When creating files use the `.py` extension.

## Adding comments to your program
Use the character `#` to start a comment. The remainder of the line will be ignored by python.

~~~~
print "This is not a comment"
# print "This is a comment"
print 3 # - 2
'''
print "the values in these lines"
print "are not printed"
print "as they are part of a multiline comment"
'''
print "got it?"
~~~~

The first line prints the string
The second line is a comment, so it is not executed. 
The third line prints the value `3`.
The next five lines are comments, delimited from `'''` to `'''`.
Finally, the last line prints `got it?`.


## Variables, values and operations

Python allows you to manipulate values which may represent integers, real numbers, and so on. The games you are about to build will need those. They will involve some game rules, and physics. More importantly, building games is actually quite fun. Are you ready? Let's start...

The easiest way to learn about variables and their values is to play a bit with them. We will build and evaluate a few expressions using those variables.

Before we begin, we need a special program, called an "interpreter". This interpreter, in this case called **python** will allow us to run programs and play with those variables and expressions. Soon enough, we will be using the interpreter to run our games!



# References

[Panda 3D documentation - Asteroids example](https://www.panda3d.org)
[Program Arcade Games With Python And Pygame](http://programarcadegames.com/index.php?lang=en)