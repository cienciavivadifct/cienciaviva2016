class Character():
    name = "Mike"
    sex = "male"
    maxHitPoints = 50
    currentHitPoints = 50
    maxSpeed = 10

    def display(self):
        print (self.name, 
               self.sex, 
               self.maxHitPoints,
               self.currentHitPoints,
               self.maxSpeed)
   
myCharacter = Character()
myCharacter2 = Character()
myCharacter2.name = "Spock"
myCharacter.display()
myCharacter2.display()
