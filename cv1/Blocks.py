# Import a library of functions called 'pygame'
import pygame
import random

# Define some colors
BLACK = (0, 0, 0)
WHITE = (255, 255, 255)
BLUE = (0, 0, 255)
GREEN = (0, 255, 0)
RED = (255, 0, 0)



#-------------------------------------------------------------------------
# Block class definition
#-------------------------------------------------------------------------

class Block(pygame.sprite.Sprite):
    def __init__(self, color, width, height):
        # Call parent constructor
        super().__init__()
        
        # Create blank surface to draw on.
        self.image = pygame.Surface([width, height]) # blank surface
        self.image.fill(color) # paint the surface
        
        self.rect = self.image.get_rect()
    
    def update(self):
        """ Called each frame. """
         
        # Move block down one pixel
        self.rect.y += 1    




# Initialize the game engine
pygame.init()

# Set screen dimensions
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600

#size = [SCREEN_WIDTH, SCREEN_HEIGHT]

screen = pygame.display.set_mode( [SCREEN_WIDTH, SCREEN_HEIGHT] )
#pygame.display.set_caption("A fantastic python game")

# Create a set of blocks with a random distribution inside the screen
blockList = pygame.sprite.Group()

spritesList = pygame.sprite.Group()

#blockWidth = 20
#blockHeight = 15

#generate all blocks
for i in range (50):
    # Create a new black block
    block = Block (BLACK, 20, 15)
    
    # Randomly generate the block's initial coordinates x and y
    block.rect.x = random.randrange(SCREEN_WIDTH)
    block.rect.y = random.randrange(SCREEN_HEIGHT)
    
    # Add the new block to both lists of sprites.
    blockList.add(block)
    spritesList.add(block)

#generate player block

player = Block (RED, 20, 15)
spritesList.add(player)

# This program will loop until the user kills the window
done = False

clock = pygame.time.Clock()
score = 0

while not done:
    # recolher input do jogador
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

    # Clear screen
    screen.fill(WHITE)
    
    # mecanica do jogo
    position = pygame.mouse.get_pos()
    
    # update all the actors
    player.rect.x = position[0]
    player.rect.y = position[1]
    
    # Check if the player collided with anything. 
    #The flag True removes hit blocks from blockList
    blocksHitList = pygame.sprite.spritecollide(player, blockList, True)
    
    for block in blocksHitList:
        score += 1
        print (score)
    
    # Call the update() method for all blocks in the block_list
    blockList.update()        
    # --------------------draw on the screen -----------------------
    
    # --------------------prepare new image ------------------------
    spritesList.draw(screen)
    
     
    # --------------------draw new image ---------------------------
    pygame.display.flip()

    clock.tick(60)
    
pygame.quit()