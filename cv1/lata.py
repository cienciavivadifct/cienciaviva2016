PI = 3.14159

def area(raio):
    areaCirculo = PI * raio ** 2 
    return areaCirculo

def perimetro(raio):
    perimetroCircunferencia = 2 * PI * raio
    return perimetroCircunferencia

def volume(raio, altura):
    volumeCilindro = area(raio) * altura
    return volumeCilindro


def areaSixPack (raio):
    areaSix = 4 * raio * 6 * raio
    return areaSix

def volumeSixPack (raio, altura):
    vol = areaSixPack(raio) * altura
    return vol
    
    
meuRaio = float(input ("Qual o raio da lata? "))
minhaAltura = float(input ("Qual a altura da lata? "))

print "Area do sixpack:", areaSixPack(meuRaio)
print "Volume do sixpack:", volumeSixPack(meuRaio, minhaAltura)
                    