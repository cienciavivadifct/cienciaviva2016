PI = 3.14159

class Lata():
#classe que representa latas.    
    def __init__(eu):
        #construtor de latas
        eu.raio = 0.0
        eu.altura = 0.0
        eu.marca = ""
        
        #posicao da lata
        eu.x = 0
        eu.y = 0
        
        #velocidade da lata
        eu.velX = 0
        eu.velY = 0
        
    def area(eu):
        areaCirculo = PI * eu.raio ** 2 
        return areaCirculo
        
    def perimetro(eu):
        perimetroCircunferencia = 2 * PI * eu.raio
        return perimetroCircunferencia
        
    def volume(eu):
        volumeCilindro = eu.area() * eu.altura
        return volumeCilindro
       
    def move(eu):
        eu.x = eu.x + eu.velX
        eu.y = eu.y + eu.velY
        
        
# Programa...

lataCocaCola = Lata()

print lataCocaCola.x, lataCocaCola.y

lataCocaCola.velX = 10
lataCocaCola.velY = -2
lataCocaCola.move()
lataCocaCola.move()
lataCocaCola.move()
print lataCocaCola.x, lataCocaCola.y
