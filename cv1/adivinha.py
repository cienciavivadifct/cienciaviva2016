import random

segredo = random.randrange(101)
jogadas = 0

tentativa = int(input("escolha um numero entre 0 e 100:"))
jogadas = jogadas + 1

while segredo != tentativa:
    if tentativa > segredo:
        print "alto demais"
    else:
        print "baixo demais"
    
    tentativa = int(input("escolha um numero entre 0 e 100:"))
    jogadas = jogadas + 1

print "parabens. conseguiu em", jogadas, "tentativas"       