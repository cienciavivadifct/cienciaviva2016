class SeaCreature():
    def __init__(self, name, x, y, speedX, speedY):
        self.name = name
        self.x = x
        self.y = y
        self.speedX = speedX
        self.speedY = speedY
        
    def swim(self):
        self.x += self.speedX
        self.y += self.speedY

class Fish(SeaCreature):
    def __init__(self, name, x, y, speedX, speedY):
        super().__init__(name, x, y, speedX, speedY)
    
    
    def report(self):
        print("I am ", self.name, "the fish, at", self.x, ",", self.y, "!")

class Shark(SeaCreature):
    def __init__(self, name, x, y, speedX, speedY):
        super().__init__(name, x, y, speedX, speedY)

    def report(self):
        print("I am ", self.name, "the shark, at", self.x, ",", self.y, "!")

seaCreatures = []

seaCreatures.append(Fish("Sam", 2,3,2,0))
seaCreatures.append(Fish("Suzy", 3,4,1,-1))
seaCreatures.append(Shark("Susana", 2, 2, 1, 1))

tim = Shark ("Tim Vieira", 0, 0, 0, 0)

tim.speedX = 1

print("Before animation")

for swimmer in seaCreatures:
    swimmer.report()

print("after animation")
# Animate:
for swimmer in seaCreatures:
    swimmer.swim()
    swimmer.report()
    
tim.swim()
tim.report()