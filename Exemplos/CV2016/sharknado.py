import random

class CriaturaMarinha():
	def __init__(self, nome, x, y, velX, velY, peso):
		self.nome = nome
		self.x = x
		self.y = y
		self.velX = velX
		self.velY = velY
		self.peso = peso
		self.tipo = "um qualquer"

	def estado(self):
		print self.tipo, self.nome, " @ ", self.x, ", ", self.y

	def nada(self):
		self.x = self.x + self.velX
		self.y = self.y + self.velY

	def posX(self): # devolve coordenada X
		return self.x

	def posY(self): # devolve coordenada Y
		return self.y


    
class Peixe(CriaturaMarinha):
	def __init__(self, nome, x, y, velX, velY, peso, cor):
		CriaturaMarinha.__init__(self, nome, x, y, velX, velY, peso)
		self.cor = cor
		self.tipo = "peixe"

	def estado(self):
		print self.nome, self.cor, self.x, self.y 



class Tubarao(CriaturaMarinha):
	def __init__(self, nome, x, y, velX, velY, peso):
		CriaturaMarinha.__init__(self, nome, x, y, velX, velY, peso)
		self.tipo = "tubarao"

	def estado(self):
		print "Sou um tubarao comilao", self.x, self.y

	def comerSoUm(self, cardume):
		cardume.comerSoUm(self.x, self.y)

	def comerTodos(self, cardume):
		cardume.comerTodos(self.x, self.y)

	def nadaPara(self, x, y):
		self.x = x
		self.y = y

	def alteraVelocidade(self):
		self.velX = input("velocidade x: ")
		self.velY = input("velocidade y: ")



class Cardume():
	def __init__(self, numeroPeixes):
		self.peixinhos = [] # Cardume vazio
		contador = 0
		while contador < numeroPeixes:
			x = random.randrange(10)
			y = random.randrange(20)
			velX = random.randrange(-2, 2)
			velY = random.randrange(-2, 2)
			peso = random.randrange(1, 5)
			self.peixinhos.append(Peixe(contador, x, y, velX, velY, peso, "laranja"))
			contador = contador + 1

	def contaPeixinhos(self):
		return len(self.peixinhos)

	def mostraPeixes(self):
		print "---------------------------"
		for umPeixinho in self.peixinhos:
			umPeixinho.estado()

	def nada(self):
		for umPeixinho in self.peixinhos:
			umPeixinho.nada()

	def comerOPrimeiro(self):
		if self.contaPeixinhos() > 0:
			del self.peixinhos[0]

	# Come um peixe que esteja em posX, posY, 
	# ou nao faz nada se nao houver peixe nessa posicao
	def comerSoUm(self, posX, posY):
		indice = 0
		fome = True
		while indice < self.contaPeixinhos() and fome:
			if self.peixinhos[indice].posX() == posX and self.peixinhos[indice].posY() == posY:
				del self.peixinhos[indice]
				fome = False
			indice = indice + 1

	# Come todos os peixes numa determinada posicao
	def comerTodos(self, posX, posY): 
		indice = self.contaPeixinhos()-1
		while indice >= 0:
			if self.peixinhos[indice].posX() == posX and self.peixinhos[indice].posY() == posY:
				del self.peixinhos[indice]
            indice = indice -1


#################################################






# Programa principal
peixeirada = Cardume(5)
bruce = Tubarao("Bruce Springsteen", 0, 3, 2, 0, 100)

while peixeirada.contaPeixinhos() > 0:
	peixeirada.mostraPeixes()
	bruce.estado()
	bruce.alteraVelocidade()
	bruce.nada()
	bruce.comerSoUm(peixeirada)
	peixeirada.nada()

print("que barrigada")
