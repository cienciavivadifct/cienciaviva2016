# Exemplo de um mapa chamado agenda.
# A chave e o nome.
# O valor e o endereco email.

agenda = {
	'vitor': 'vad@fct.unl.pt',
	'miguel': 'mgoul@fct.unl.pt',
	'jose': 'jose@campus.fct.unl.pt',
	'lara': 'lara@croft.com' 
}

print "A agenda tem", len(agenda), "contactos"

nome = input ("Que endereco email quer? ")

if nome in agenda:
	print "O email que procura e: ", agenda[nome]
else:
	print "Nao temos o endereco de ", nome