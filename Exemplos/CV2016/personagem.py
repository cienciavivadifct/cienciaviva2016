# Exemplo para mostrar a sintaxe de uma classe
# A classe tem o nome Personagem
# Atributos:
# - nome - nome do personagem
# - pontos - pontos de vida do personagem; se <= 0, o personagem esta morto.

class Personagem():
	# Construtor de objectos do tipo Personagem
	def __init__(self, nome, pontos):
		self.nome = nome
		self.pontos = pontos

	# Selector para o nome
	def devolveNome(self):
		return self.nome

	# Selector para os pontos
	def devolvePontos(self):
		return self.pontos

	# Selector para testar se o personagem esta vivo
	def estaVivo(self):
		return self.pontos > 0

	# Modificador para bater no personagem, fazendo-o perder pontos de vida 
	def bate(self, n):
		self.pontos = self.pontos - n


###############################
# Programa principal, para ilustrar a criacao, consulta e modificacao do estado dos objectos
###############################

# Construcao de instancias
rocky = Personagem("Balboa", 100)
appolo = Personagem("Appolo Creed", 1000) 
mike = Personagem("Mike Tyson", 200)

# Consulta ao estado dos objectos
print rocky.devolveNome()
print appolo.devolveNome()

print rocky.devolvePontos()
print appolo.devolvePontos()

# Modifica o estado dos varios personagens
rocky.bate(20)
rocky.bate(20)
rocky.bate(20)
rocky.bate(20)
appolo.bate(300000)

# Novas consultas ao estado dos varios personagens
print rocky.devolvePontos()
print appolo.devolvePontos()
print rocky.estaVivo()
print appolo.estaVivo()