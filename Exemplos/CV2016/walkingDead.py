VIVO = 0
ZOMBIE = 1
MORTO = 2

class Pessoa():
	def __init__(self, nome, estado):
		self.nome = nome
		self.estado = estado

	def falar(self):
		if self.estado == VIVO:
			print "Estou vivinho da Silva e o meu nome e", self.nome
		elif self.estado == ZOMBIE:
			print "iuoytrewgjreglmvv"
		else:
			print "..."

	def mordido(self):
		if self.estado == VIVO:
			self.estado = ZOMBIE

	def curado(self):
		if self.estado == ZOMBIE:
			self.estado = VIVO

	def morto(self):
		if self.estado == VIVO or self.estado ==ZOMBIE:
			self.estado = MORTO

	def melisandrar(self):
		self.estado = VIVO

laura = Pessoa("Laura Palmer", MORTO)
zeca = Pessoa("zeca diabo", ZOMBIE)
laura.falar()
zeca.falar()
zeca.mordido()
zeca.falar()
zeca.curado()
zeca.falar()
zeca.morto()
zeca.falar()
laura.falar()