def imprime(lista):
	for item in lista:
		print item	

def imprimeComWhile(lista):
	indice = 0
	while indice < len(lista):
		print lista[indice]
		indice = indice + 1 

def imprimeAlfabeticaInversa(lista):
	lista.sort()
	indice = len(lista) - 1
	while indice >= 0:
		print lista[indice]
		indice = indice - 1

def compra(lista, indice):
	itemComprado = lista[indice]
	del lista[indice]
	return itemComprado

def apaga(lista):
	while len(lista) > 0:
		del lista[0]

##########################

listaCompras = ["banana", "tv", "pera","cebola", "cenoura"]

print "imprime com for"
imprime(listaCompras)

# O exemplo com while serve para mostrar como o for each pode ser feito com um while
#print "\nimprime com while"
#imprimeComWhile(listaCompras)

print "faz o resto..."
listaCompras.append("bimby")


print "comprei " , compra(listaCompras, 3)

imprime(listaCompras)

listaCompras.sort()

imprime(listaCompras)

print "primeiro elemento da lista de compras: ", listaCompras[1]