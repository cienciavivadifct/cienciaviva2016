VIVO = 0
ZOMBIE = 1
MORTO = 2

class PersonagemWalkingDead():
    def __init__(self, nome, peso):
        self.nome = nome
        self.estado = VIVO
        self.peso = peso
        
    def falar(self):
        if self.estado == VIVO:
            print ("Ola, eu sou o " + self.nome + "e peso " + str(self.peso))
        elif self.estado == ZOMBIE:
            print ("ghhhgggergrhgjh" + self.nome + "e peso "+ str(self.peso))
        else:
            print ("morri" +self.nome + str(self.peso))
    
    def morder(self):
        if self.estado == VIVO:
            self.estado = ZOMBIE
        self.peso -= 1
    
    def morrer(self):
        if self.estado == VIVO or self.estado == ZOMBIE:
            self.estado = MORTO
    
    def curar(self):
        if self.estado == ZOMBIE:
            self.estado = VIVO


carl = PersonagemWalkingDead("Carl", 90)
joe = PersonagemWalkingDead("Joe", 80)
lois = PersonagemWalkingDead("Lois",57)

joe.falar()
carl.falar()
lois.falar()
lois.morder()
lois.falar()
joe.falar()
carl.morrer()
carl.falar()
joe.falar()
lois.falar()
lois.curar()
lois.falar()
carl.morder()
carl.falar()