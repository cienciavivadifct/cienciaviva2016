import random

segredo = random.randrange(101)

# outra forma de fazer o random, especificando limites inferior e superior.
#segredo = random.randrange(0,100)

contador = 0


tentativa = int (input("Por favor, tente um numero entre 0 e 100: "))
contador = contador + 1

while tentativa != segredo:
    if tentativa < segredo:
        print "demasiado baixo"
    else:
        print "demasiado alto"
    
    tentativa = int (input("Por favor, tente um numero entre 0 e 100: "))
    contador = contador + 1

print "Acertou no numero", segredo, "ao fim de", contador, "tentativas!"  