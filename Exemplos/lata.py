PI = 3.14159

# Cilindro
class Cilindro():
    def __init__(self, novoRaio, novaAltura):
        self.raio = novoRaio
        self.altura = novaAltura
        
    def area (self):
        return PI * self.raio ** 2

    def volume (self):
        return self.area() * self.altura
    
    def estica (self, factor):
        self.altura = self.altura * factor
        self.raio = self.raio * factor

# Paralelipipedo:
class Paralelipipedo():
    def __init__(self, largura, comprimento, altura):
        self.largura = largura
        self.comprimento = comprimento
        self.altura = altura
        
    def area(self):
        return self.largura * self.comprimento

    def volume(self):
        return self.area() * self.altura

    def estica (self, factor):
        self.comprimento *= factor
        self.largura *= factor
        self.altura *= factor
        
volumeTotal = 0.0
resposta = input ("Deseja guardar mais objectos? ")

while resposta != "nao":
    objecto = input("Que objecto quer guardar? ")
    if objecto == "lata":
        raioLata = float(input ("Qual o raio da lata?"))
        alturaLata = float (input("Qual a altura da lata?"))
        lata = Cilindro (raioLata, alturaLata)
        volumeTotal += lata.volume()
    elif objecto == "pacote":
        largura = float (input("Qual e a largura? "))
        comprimento = float (input ("Qual e o comprimento? "))
        altura = float (input("Qual e a altura? "))
        pacote = Paralelipipedo(largura, comprimento, altura)
        volumeTotal += pacote.volume()
        
    else:
        print ("Objecto desconhecido")
    
    resposta = input ("Deseja guardar mais objectos? ")

print ("Volume total = ", volumeTotal)    