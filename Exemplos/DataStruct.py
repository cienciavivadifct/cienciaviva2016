# LISTS   /   SEQUENCES (arrays)
# This is my shopping list
shoplist = ['apple', 'mango', 'carrot', 'banana']

print 'I have', len(shoplist), 'items to purchase.' 

print 'These items are:' 
for item in shoplist:
    print item, ' '

print '\nI also have to buy rice.' 
shoplist.append('rice')
print 'My shopping list is now', shoplist

print 'I will sort my list now' 
shoplist.sort()
print 'Sorted shopping list is', shoplist 

print 'The first item I will buy is', shoplist[0] 
olditem = shoplist[0]
del shoplist[0]
print 'I bought the', olditem 
print 'My shopping list is now', shoplist 

#---
#shoplist = ['apple', 'mango', 'carrot', 'banana']

# Indexing or 'Subscription' operation #
print 'Item 0 is', shoplist[0] 
print 'Item 1 is', shoplist[1] 
print 'Item 2 is', shoplist[2] 
print 'Item 3 is', shoplist[3] 
print 'Item -1 is', shoplist[-1] 
print 'Item -2 is', shoplist[-2] 

name = 'swaroop'
print 'Character 1 is', name[1] 


# DICTIONARY
# 'ab' is short for 'a'ddress'b'ook
ab = {
    'Swaroop': 'swaroop@swaroopch.com',
    'Larry': 'larry@wall.org',
    'Matsumoto': 'matz@ruby-lang.org',
    'Spammer': 'spammer@hotmail.com'
}

print "Swaroop's address is", ab['Swaroop'] 

# Deleting a key-value pair
del ab['Spammer']

print '\nThere are {} contacts in the address-book\n'.format(len(ab)) 

for name, address in ab.items():
    print 'Contact {} at {}'.format(name, address) 

# Adding a key-value pair
ab['Guido'] = 'guido@python.org'

if 'Guido' in ab:
    print "\nGuido's address is", ab['Guido'] 


