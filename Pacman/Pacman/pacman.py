import pygame
import constants
import colors

pygame.init()
 
# Set the width and height of the screen [width, height]

size = (constants.SCREEN_WIDTH, constants.SCREEN_HEIGHT)
screen = pygame.display.set_mode(size)
pygame.display.set_caption("Pac Man")

import spritesheet
from player import Player
from levels import Level
 
# Loop until the user clicks the close button.
done = False
 
# Used to manage how fast the screen updates
clock = pygame.time.Clock()

player = Player(104,108) # BUG HERE! (was 104,104)
level = Level(0)

sprites_list = level.build()

edibles = pygame.sprite.Group()
powerups = pygame.sprite.Group()
obstacles = pygame.sprite.Group()
killers = pygame.sprite.Group()
movables = pygame.sprite.Group()
all = pygame.sprite.Group()

for sprite in sprites_list:
    if sprite.is_edible():
        edibles.add(sprite)
    if sprite.is_powerup():
        powerups.add(sprite)
    if sprite.is_killer():
        killers.add(sprite)
    if sprite.is_obstacle():
        obstacles.add(sprite)
    if sprite.is_movable():
        movables.add(sprite)
    all.add(sprite)

all.add(player)

# -------- Main Program Loop -----------
while not done:
    # --- Main event loop
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True
        if(player != None):
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_UP:
                    player.move_up()
                elif event.key == pygame.K_DOWN:
                    player.move_down()
                elif event.key == pygame.K_LEFT:
                    player.move_left()
                elif event.key == pygame.K_RIGHT:
                    player.move_right()
            
            
 
    # --- Game logic should go here
    all.update(edibles, powerups, killers, obstacles, movables)
 
    # --- Drawing code should go here
 
    # First, clear the screen to white. Don't put other drawing commands
    # above this, or they will be erased with this command.
    screen.fill(colors.BLACK)
 
    all.draw(screen)
    #level.draw(screen)

    # --- Go ahead and update the screen with what we've drawn.
    pygame.display.flip()
 
    # --- Limit to FPS frames per second
    clock.tick(constants.FPS)
 
# Close the window and quit.
# If you forget this line, the program will 'hang'
# on exit if running from IDLE.
pygame.quit()


