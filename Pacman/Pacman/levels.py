
""" All things related to the levels of the game """

import pygame
from spritesheet import SpriteSheet

class Actor(pygame.sprite.Sprite):
    def __init__(self, x, y, image):
        super().__init__()
        self.image = image
        self.rect = image.get_rect()
        self.rect.x = x
        self.rect.y = y

    def is_edible(self):
        return False

    def is_obstacle(self):
        return False

    def is_killer(self):
        return False

    def is_movable(self):
        return False

    def is_powerup(self):
        return False

class Wall(Actor):
    def __init__(self, x, y, image):
        super().__init__(x,y,image)

    def is_obstacle(self): return True

class Monster(Actor):
    def __init__(self, x, y, image):
        super().__init__()

    def is_killer(self):
        return True

    def is_movable(self):
        return True


class Food(Actor):
    FOOD_POINTS = 1
    def __init__(self, x, y, image):
        super().__init__(x,y,image)

    def is_edible(self): return True

    def eat(self): return self.FOOD_POINTS

class MonsterKiller(Food):
    FOOD_POINTS = 10
    def __init__(self, x, y, image):
        super().__init__(x,y,image)

    def eat(self): return self.FOOD_POINTS

    def is_powerup(self): return True


# the list of levels
LEVELS = [
    [ 
        "qwwwwwwwwwwwweqwwwwwwwwwwwwe",
        "a............aa............a",
        "a.qwwe.qwwwe.aa.qwwwe.qwwe.a",
        "aOa  a.a   a.aa.a   a.a  aOa",
        "a.zwwc.zwwwc.zc.zwwwc.zwwc.a",
        "a..........................a",
        "a.qwwe.qe.qwwwwwwe.qe.qwwe.a",
        "a.zwwc.aa.zwweqwwc.aa.zwwc.a",
        "a......aa....aa....aa......a",
        "zwwwwe.azwwe aa qwwca.qwwwwc",
        "     a.aqwwc zc zwwea.a     ",
        "     a.aa          aa.a     ",
        "     a.aa qwwwwwwe aa.a     ",
        "wwwwwc.zc a      a zc.zwwwww",
        "      .   a      a   .      ",
        "wwwwwe.qe a      a qe.qwwwww",
        "     a.aa zwwwwwwc aa.a     ",
        "     a.aa          aa.a     ",
        "     a.aa qwwwwwwe aa.a     ",
        "qwwwwc.zc zwweqwwc zc.zwwwwe",
        "a............aa............a",
        "a.qwwe.qwwwe.aa.qwwwe.qwwe.a",
        "a.zwea.zwwwc.zc.zwwwc.aqwc.a",
        "aO..aa.......  .......aa..Oa",
        "zwe.aa.qe.qwwwwwwe.qe.aa.qwc",
        "qwc.zc.aa.zwweqwwc.aa.zc.zwe",
        "a......aa....aa....aa......a",
        "a.qwwwwczwwe.aa.qwwczwwwwe.a",
        "a.zwwwwwwwwc.zc.zwwwwwwwwc.a",
        "a..........................a",
        "zwwwwwwwwwwwwwwwwwwwwwwwwwwc"
    ]
]

def build_images():
    sheet = SpriteSheet("images/levelitems.png")
    items = {}

    items["q"] = sheet.get_image(16,16,8,8)
    items["w"] = sheet.get_image(24,16,8,8)
    items["a"] = sheet.get_image(16,24,8,8)
    items["."] = sheet.get_image(8,8,8,8)
    items["e"] = sheet.get_image(40,16,8,8)
    items["O"] = sheet.get_image(8,24,8,8)
    items[" "] = sheet.get_image(24,24,8,8)
    items["z"] = sheet.get_image(16,32,8,8)
    items["c"] = sheet.get_image(40,32,8,8)
    return items

def build_actor(kind, x, y, image):
    # Bug here! z was another character, so the maze would not render low left corners!
    if kind=="q" or kind=="w" or kind=="a" or kind == "e" or kind == "z" or kind == "c": return Wall(x,y,image)
    elif kind==".": return Food(x,y,image)
    elif kind=="O": return MonsterKiller(x,y,image)
    elif kind==" ": return None

class Level:
    # static member variables
    images = build_images()

    def __init__(self, level):
        self.level = level
        self.map = LEVELS[self.level]

    def build(self):
        sprites = []
        for i in range(0,len(self.map)):
            for j in range(0,len(self.map[i])):
                kind = self.map[i][j]
                sprite = build_actor(kind, 8*j, 8*(i+3), self.images[kind])
                if sprite != None: sprites.append(sprite)

        return sprites

    def draw(self,screen):
        for i in range(0,len(self.map)):
            for j in range(0,len(self.map[i])):
                img = self.images[self.map[i][j]]
                screen.blit(img, [8*j, 8*(i+3)])