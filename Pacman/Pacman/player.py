import pygame
from spritesheet import SpriteSheet

PACMAN_RIGHT = 0
PACMAN_LEFT = 1
PACMAN_UP = 2
PACMAN_DOWN = 3
PACMAN_DYING = 4

#class AnimatedSprite(pygame.sprite.Sprite):


class Player(pygame.sprite.Sprite):
	# static member variables
	pacman_frames = [ [], [], [], [], [] ]
	xSpeed = 0
	ySpeed = 0

	def __init__(self, x, y):
		super().__init__()
		sprite_sheet = SpriteSheet("images/spritesheet.png")

		self.pacman_frames[PACMAN_RIGHT].append(sprite_sheet.get_image(0, 0, 16, 16))
		self.pacman_frames[PACMAN_RIGHT].append(sprite_sheet.get_image(16, 0, 16, 16)) # Bug here!! (was 16, 0, 16, 10)

		self.pacman_frames[PACMAN_LEFT].append(sprite_sheet.get_image(0, 16, 16, 16))
		self.pacman_frames[PACMAN_LEFT].append(sprite_sheet.get_image(16, 16, 16, 16))

		self.pacman_frames[PACMAN_UP].append(sprite_sheet.get_image(0, 32, 16, 16))
		self.pacman_frames[PACMAN_UP].append(sprite_sheet.get_image(16, 32, 16, 16))

		self.pacman_frames[PACMAN_DOWN].append(sprite_sheet.get_image(0, 48, 16, 16))
		self.pacman_frames[PACMAN_DOWN].append(sprite_sheet.get_image(16, 48, 16, 16))

		for i in range(2,14):
			self.pacman_frames[PACMAN_DYING].append(sprite_sheet.get_image(32+i*16, 0, 16, 16))

		#self.start_animation(PACMAN_DYING)
		self.start_animation(PACMAN_RIGHT)
		self.rect = self.image.get_rect()
		self.rect.x = x
		self.rect.y = y
		self.rect.inflate(-8,-8)
		
		# New variables
		self.xSpeed = 0
		self.ySpeed = 0

	def start_animation(self, state):
		self.state = state
		self.img_index = 0
		self.update_image()

	def update_image(self):
		self.image = self.pacman_frames[self.state][self.img_index]

	def is_animation_finished(self):
		return self.img_index == len(self.pacman_frames[self.state]) - 1

	def advance_frame(self):
		self.img_index = (self.img_index + 1) % len(self.pacman_frames[self.state])
		self.update_image()

	def move_up(self):
		self.start_animation(PACMAN_UP)
		self.xSpeed = 0
		self.ySpeed = -4

	def move_down(self):
		self.start_animation(PACMAN_DOWN)
		self.xSpeed = 0
		self.ySpeed = 4

	def move_left(self):
		self.start_animation(PACMAN_LEFT)
		self.xSpeed = -4
		self.ySpeed = 0

	def move_right(self):
		self.start_animation(PACMAN_RIGHT)
		self.xSpeed = 4
		self.ySpeed = 0
		
	def stop_horizontal(self):
		self.xSpeed = 0
	
	def stop_vertical(self):
		self.ySpeed = 0

	def update(self, edibles, powerups, killers, obstacles, movables):
		# Deal with movement...
		# Move left/right
		self.rect.x += self.xSpeed
		
		# Did this update cause us to hit an obstacle?
		wallsHitList = pygame.sprite.spritecollide(self, obstacles, False)
		for wall in wallsHitList:
			print ("horizontal hit at", self.xSpeed, self.rect.right, wall.rect.left)
			if self.xSpeed > 0: # We hit a wall on its left side.
				self.rect.right = wall.rect.left
			elif self.xSpeed < 0: # We hit a wall on its right side.
				self.rect.left = wall.rect.right
			self.xSpeed = 0
		
		
		# Move up/down
		self.rect.y += self.ySpeed
		wallsHitList = pygame.sprite.spritecollide(self, obstacles, False)
		for wall in wallsHitList:
			print ("vertical hit at", self.ySpeed)
			if self.ySpeed > 0: # We hit a wall on its top side.
				self.rect.bottom = wall.rect.top
			elif self.ySpeed < 0: # We hit a wall on its bottom side.
				self.rect.top = wall.rect.bottom
			self.ySpeed = 0
				

		# Deal with the change in image (sprite animation)...
		#if(self.state == PACMAN_DYING):
		self.advance_frame()

