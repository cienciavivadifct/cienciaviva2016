import pygame

""" Classe base para representar blocos coloridos """

class Block(pygame.sprite.Sprite):
	""" Classe para representar blocos coloridos """
	def __init__(self, x, y, width, height, color):
		super().__init__()
		self.image = pygame.Surface([width, height])
		self.image.fill(color)
		self.rect = self.image.get_rect()
		self.rect.x = x
		self.rect.y = y