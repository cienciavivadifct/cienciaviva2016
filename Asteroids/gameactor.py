from panda3d.core import NodePath
from panda3d.core import LPoint2, LVector2
from panda3d.core import LPoint3
from panda3d.core import TransparencyAttrib

class GameActor(NodePath):
    def __init__(self, tex=None, pos=LPoint2(0,0), size=LVector2(32,32),transparency=True, depth=0.5):
        obj = loader.loadModel("models/plane")
        NodePath.__init__(self, obj)
        self.reparentTo(camera)
        
        # Set the initial position and scale of the actor
        self.setPos(pos.getX(), depth, pos.getY())
        self.setScale(size.getX(), 1, size.getY())
        
        self.setBin("unsorted", 0)
        self.setDepthTest(False)
        
        if transparency:
            # Enable transparency blending
            self.setTransparency(TransparencyAttrib.MAlpha)
            
        if tex:
            # Load and set the requested texture
            img = loader.loadTexture("textures/" + tex)
            self.setTexture(img, 1)
            
        self.setVelocity(LPoint2(0.0,0.0))
        self.time = 0
        self.expires = -1

    # sets the current position
    def setPosition(self, pos):
        newPos = LPoint3(pos.getX(), self.getPos().getY(), pos.getY())
        self.setPos(newPos)

    # return the current position
    def getPosition(self):
        return LPoint2(self.getPos().getX(), self.getPos().getZ())

    # sets the current velocity vector
    def setVelocity(self, val):
        self.velocity = LPoint3(val.getX(), 0, val.getY())
        
    # returns the current velocity vector
    def getVelocity(self):
        return LPoint2(self.velocity.getX(), self.velocity.getZ())
    
    # returns the width of the actor
    def getWidth(self):
        return self.getScale().getX()

    # returns the width of the actor
    def getHeight(self):
        return self.getScale().getZ()

    # sets the time of expiration for the actor
    def setExpires(self, val):
        self.expires = val
    
    # gets the time of expiration of the actor
    def getExpires(self):
        return self.expires

    # Returns true if the expiration time has already passed. Zero means no expiration
    def isExpired(self):
        return self.expires > 0 and self.expires < self.time

    # kill an actor by setting its expiracy to the actual time
    def kill(self):
        self.expires = self.time

    # Checks if an actor is overlapping another
    def collidesWith(self, other):

        myX = self.getPosition().getX()
        myY = self.getPosition().getY()
        myHWidth = self.getWidth()/2
        myHHeight = self.getHeight()/2

        otherX = other.getPosition().getX()
        otherY = other.getPosition().getY()
        otherHWidth = other.getWidth()/2
        otherHHeight = other.getHeight()/2

        # Check for no overlap in X direction
        if myX - myHWidth > otherX + otherHWidth:
            return False
        if myX + myHWidth < otherX - otherHWidth:
            return False

        # Check for no overlap in Y direction
        if myY - myHHeight > otherY + otherHHeight:
            return False
        if myY + myHHeight < otherY - otherHHeight:
            return False

        # If we are here, it means a simultaneous overlap both in X and Y directions, thus a real overlap...
        return True

    def update(self, dt, game):
        # update internal time for actor
        self.time += dt
        # updates its position according to its velocity
        self.updatePos(dt)

    # Notification that the position has changed so that the actor can react to it
    def posUpdated(self):
        pass
    
    # Updates the posision of the actor in accordance to its speed and elapsed time
    def updatePos(self, dt):
        vel = self.velocity
        newPos = self.getPos() + (vel * dt)
        self.setPos(newPos)
        # At the end, let the object be notified of its position change
        self.posUpdated()
