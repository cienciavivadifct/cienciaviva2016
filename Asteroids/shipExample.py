from math import sin, cos, pi

DEG_TO_RAD = pi / 180



class Ship():
    def __init__(self,x,y,v,dir):
        self.x = x
        self.y = y
        self.v = v
        self.dir = dir

    def move(self, dt):
        self.x = self.x + self.v*cos(self.dir*DEG_TO_RAD)*dt
        self.y = self.y + self.v*sin(self.dir*DEG_TO_RAD)*dt

    def show(self):
        print self.x
        print self.y

    def turn(self,deg):
        self.dir = self.dir + deg
        
ship = Ship(10,20,5,30)
ship.show()
ship.move(10)
ship.show()
ship.turn(90)
ship.move(10)
ship.show()