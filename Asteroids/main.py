from asteroidsgame import AsteroidsGame

# Let us create an object from our AsteroidsDemo class
demo = AsteroidsGame()
# and ask it to run
demo.run()
