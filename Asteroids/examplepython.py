from math import sin, cos, pi
DEG_TO_RAD = pi / 180
x = 10
y = 20
v = 5
dir = 30

def move(dt):
    global x
    global y
    global v
    global dir

    x = x + v*cos(dir*DEG_TO_RAD)*dt
    y = y + v*sin(dir*DEG_TO_RAD)*dt

print x
print y
move(10)
move (20)
print x
print y