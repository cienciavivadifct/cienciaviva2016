from direct.showbase.ShowBase import ShowBase
from direct.task.Task import Task
from panda3d.core import LPoint2
from panda3d.core import OrthographicLens

from gameactor import GameActor

# Constants for default screen size
DEFAULT_WIDTH = 800
DEFAULT_HEIGHT = 600

NEAR_VAL = 0
FAR_VAL = 1

class Game2D(ShowBase):

    # constant that needs to be returned by the gameloop function to keep the gameloop running
    cont = Task.cont

    def __init__(self, width=DEFAULT_WIDTH, height=DEFAULT_HEIGHT):
        ShowBase.__init__(self)

        # We use an Orthographic projection for 2D
        lens = OrthographicLens()

        # And set the film size to work with pixel units,
        # or whatever is appropriate for your scene
        lens.setFilmSize(width, height)

        # Adjust the offset so that bottom-left corner is at (0,0)
        # and top-right at (SCREEN_WIDTH,SCREEN_HEIGHT)
        lens.setFilmOffset(width/2,height/2)

        # Near and far plane values are adjusted to include Y=0
        lens.setNearFar(NEAR_VAL,FAR_VAL)

        # Updates the camera node of our scence
        base.cam.node().setLens(lens)
        
        self.gameTask = taskMgr.add(self.topLevelGameLoop, "gameLoop")
        self.width = width
        self.height = height
        
        # Map of actors organized in buckets. Buckets are simply labels that we use to
        # group actors in logical groups. For instance self.actors["obstacle"] would be the list
        # of all the actors that are labeled as obstacles.
        self.actors = {}
        
    # Adds an actor to a bucket (collection with an associated label)
    def addActor(self, bucket, actor):
        # Check if it is the first actor being added to the bucket
        if bucket not in self.actors.keys():
            # Since it is the first, we first create an empty list for this bucket
            self.actors[bucket] = []
        # Add the actor to the list associated with the provided bucket
        self.actors[bucket].append(actor)
        
    # Returns the list of all the actors from the given bucket
    def getActorsFromBucket(self, bucket):
        # Check if the bucket does not even exist
        if bucket not in self.actors.keys():
            return None
        # If we reach here, the bucket exists
        # Note: it is possible that the bucket is empty and an empty list will be returned in that case
        # The list() forces a copy of the existing list so that the original one can be freely manipulated
        # without worrying what is being done with the copy elsewhere.
        return list(self.actors[bucket])

    # game loop for a specific game. Derived classes will probably override this to
    # do whatever is needed in a particular game. We simply return Task.cont to signal
    # that the game is to continue running
    def gameLoop(self, task, dt):
        return cont

    # This function gets called from the top level game loop and will request
    # each actor from each bucket to update itself. The elapsed time dt is provided
    # to the actor being updated.
    def updateAllActors(self, dt):
        for key in self.actors.keys():      # iterate over all buckets
            for actor in self.actors[key]:    # iterate over all actors in the current bucket
                actor.update(dt, self)        # request the actor to update itself according to its specific rules

    # This is the outermost game loop
    # Every game will execute this common game loop. After the tasks here,
    # specific tasks from a specific game will be executed by calling
    # gameloop() method. This method will need to return Game2D.cont to keep the
    # game running.
    def topLevelGameLoop(self, task):

        # Get the delta time that has elapsed since we were last here
        dt = globalClock.getDt()
        
        # Update all actors based on the time that has elapsed
        self.updateAllActors(dt)

        # Check for actors that have expired (reached the end of life)
        # Those will be removed from their respective buckets.
        self.removeExpiredActors()
    
        return self.gameLoop(task, dt)
    
        
    def setBackground(self, filename):
        bg = GameActor(filename, LPoint2(self.width/2,self.height/2), LPoint2(self.width,self.height),False,0.6)

    # Go through all buckets and for each bucket go through all its actors
    # Non expired actors are placed in the list of those that will survive to the next stage
    # Expired actors are not copied to the new lists
    def removeExpiredActors(self):
        for key in self.actors.keys():
            notExpired = []
            for a in self.actors[key]:
                if a.isExpired():
                    a.removeNode()
                else:
                    notExpired.append(a)
            self.actors[key] = notExpired
