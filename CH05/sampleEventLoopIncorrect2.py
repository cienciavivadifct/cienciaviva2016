# Sample event loop - Incorrect!

for event in pygame.event.get():
    if event.type == pygame.QUIT:
        print("User asked to quit.")
    elif event.type == pygame.KEYDOWN:
        print("User pressed a key.")
 
# Now we exited the for loop where events were being processed.
# Let's do something...
pygame.draw.rect(screen, GREEN, [50,50,100,100])
 
# and now, let's try to go back to event processing.
# This is code that processes events. But it is not in the
# 'for' loop that processes events. It will not act reliably.
if event.type == pygame.KEYUP:
    print("User let go of a key.")
elif event.type == pygame.MOUSEBUTTONDOWN:
    print("User pressed a mouse button")