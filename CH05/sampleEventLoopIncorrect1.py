# Sample event loop - Incorrect!

# Here is one event loop
for event in pygame.event.get():
    if event.type == pygame.QUIT:
        print("User asked to quit.")
    elif event.type == pygame.KEYDOWN:
        print("User pressed a key.")
    elif event.type == pygame.KEYUP:
        print("User let go of a key.")
 
# Here the programmer has copied another event loop
# into the program. This is BAD. The events were already
# processed. The events in this second loop will be IGNORED!
for event in pygame.event.get():
    if event.type == pygame.QUIT:
        print("User asked to quit.")
    elif event.type == pygame.MOUSEBUTTONDOWN:
        print("User pressed a mouse button")