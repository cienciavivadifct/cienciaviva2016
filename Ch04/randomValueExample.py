import random

#Random number from 0 to 49
number = random.randrange(50)
print number

#Random number from 100 to 200
otherNumber = random.randrange(100, 201)
print otherNumber

#Picking a random item out of a list
my_list = ["rock", "paper", "scissors"]
random_index = random.randrange(3)
print my_list[random_index]

#Random floating point number from 0 to 1
my_number = random.random()

#Random floating point number between 10 and 15
my_number = random.random() * 5 + 10