def volumeCylinder (radius, height):
    pi = 3.14159
    return 2*pi*radius*(radius+height)

def volumeSphere(radius):
    pi = 3.14159
    return 4 * pi * radius ** 2 # volume

# Main program   
for i in range(20):
    print(volumeCylinder(i,5))

